package chat_queue_backend

import grails.converters.JSON;
import static org.springframework.http.HttpStatus.*;

//todo: not being used for now
class QueueAttributeController {

    def index() {

        def resultSet = [:];

        log.info "Going to Fetch Queue Attributes List with Params: ${params}"

        try {

            def queueAttributes = QueueAttribute.all;

            if (queueAttributes.size() > 0) {

                resultSet.put("status", OK.name());
                resultSet.put("queueAttributes", queueAttributes);

                log.info("Queue Attributes Loaded Successfully: ${queueAttributes.size()}");

            } else {

                resultSet.put("status", NOT_FOUND.name());
                resultSet.put("message", "No Queue Attribute Found.");

                log.info("No Queue Attribute Found.");
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Loading Queue Attributes. See Error Logs for More Details.");
            log.error("Exception Occurred while Loading Queue Attributes: ", ex);
        }

        render resultSet as JSON
        return;
    }
}