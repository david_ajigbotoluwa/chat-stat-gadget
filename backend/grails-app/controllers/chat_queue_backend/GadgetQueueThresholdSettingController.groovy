package chat_queue_backend

import grails.converters.JSON;
import static org.springframework.http.HttpStatus.*;

class GadgetQueueThresholdSettingController {

    def index() {

        def resultSet = [:];

        log.info "Going to get Queue Gadget Settings with Params: ${params}"

        try {

            if (params.gadgetId) {

                String gadgetIdStr = params.gadgetId;
                int gadgetId = Integer.parseInt(gadgetIdStr);

                def applicationSetting = GadgetQueueThresholdSetting.findByGadgetId(gadgetId);

                if (applicationSetting) {

                    log.info("Queue Gadget Settings Fetched Successfully.");

                    resultSet.put("applicationSetting", applicationSetting);
                    resultSet.put("status", OK.name());
                } else {
                    log.info("Queue Gadget Settings Not Found Against Gadget Id: + ${params.gadgetId}");

                    resultSet.put("message", "Application Settings Not Found Against Gadget Id: ${params.gadgetId}");
                    resultSet.put("status", NOT_FOUND.name());
                }

            } else {

                log.info("Gadget Id Not Found in the Request while fetching Queue Gadget Settings.");

                resultSet.put("message", "Gadget Id Not Found in the Request while fetching Queue Gadget Settings.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Fetching Queue Gadget Settings. See Error Logs for More Details.");
            log.error("Occurred while Queue Gadget Settings", ex);
        }

        render resultSet as JSON
        return;
    }

    def saveSetting() {

        def resultSet = [:];

        println("Got Request to Save Queue Gadget Settings with Params: ${params}");
        log.info("Got Request to Save Queue Gadget Settings with Params: ${params}");

        try {

            if (params.gadgetId) {

                if (params.queuedcontact || params.activecontact || params.notReadyAgents || params.agentsLoggedIn || params.agentsReady)
                {

                    String gadgetIdStr = params.gadgetId;
                    int gadgetId = Integer.parseInt(gadgetIdStr);

                    def applicationSetting = GadgetQueueThresholdSetting.findByGadgetId(gadgetId);

                    if (applicationSetting == null) {

                        applicationSetting = new GadgetQueueThresholdSetting();
                        applicationSetting.gadgetId = gadgetId;

                        log.info("Queue Gadget Settings Not Found with Gadget Id: {$gadgetId}. So, Creating New Instance.")
                    }

                    String queuedcontactStr = params.queuedcontact;
                    String activecontactStr = params.activecontact;
                    String agentsReadyStr = params.agentsReady;
                    String thresholdColor = params.thresholdColor;
                    String agentsLoggedInStr = params.agentsLoggedIn;
                    String notReadyAgentsStr = params.notReadyAgents;

                    if (queuedcontactStr != null && !queuedcontactStr.isEmpty()) {
                        applicationSetting.queuedcontact = Integer.parseInt(queuedcontactStr);
                    } else {
                        applicationSetting.queuedcontact = -1;
                    }

                    if (activecontactStr != null && !activecontactStr.isEmpty()) {
                        applicationSetting.activecontact = Integer.parseInt(activecontactStr);
                    } else {
                        applicationSetting.activecontact = -1;
                    }

                    if (agentsReadyStr != null && !agentsReadyStr.isEmpty()) {
                        applicationSetting.agentsReady = Integer.parseInt(agentsReadyStr);
                    } else {
                        applicationSetting.agentsReady = -1;
                    }

                    if (notReadyAgentsStr != null && !notReadyAgentsStr.isEmpty()) {
                        applicationSetting.notReadyAgents = Integer.parseInt(notReadyAgentsStr);
                    } else {
                        applicationSetting.notReadyAgents = -1;
                    }

                    if (agentsLoggedInStr != null && !agentsLoggedInStr.isEmpty()) {
                        applicationSetting.agentsLoggedIn = Integer.parseInt(agentsLoggedInStr);
                    } else {
                        applicationSetting.agentsLoggedIn = -1;
                    }

                    if (thresholdColor != null && !thresholdColor.isEmpty()) {
                        applicationSetting.thresholdColor = thresholdColor;
                    } else {
                        applicationSetting.thresholdColor = "red";
                        log.info("Threshold Color Not Specified in the Request Parameters. So, setting 'Red' as Default Color.")
                    }

                    applicationSetting.save(flush: true, failOnError: true);

                    resultSet.put("status", OK.name());
                    resultSet.put("applicationSetting", applicationSetting);
                    resultSet.put("message", "Queue Gadget Settings Saved Successfully.");
                    log.info("Queue Gadget Settings Saved Successfully.");


                } else {
                    resultSet.put("status", BAD_REQUEST.name());
                    resultSet.put("message", "Queue Gadget Setting Parameters Not Found In the Request to Save Settings.");
                    log.info("Queue Gadget Setting Parameters Not Found In the Request to Save Settings.");
                }

            } else {

                log.info("Gadget Id Not Found in the Request while Saving Queue Gadget Settings.");

                resultSet.put("message", "Gadget Id Not Found in the Request while Saving Queue Gadget Settings.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());
            resultSet.put("message", "Some Error Occurred while Saving Queue Gadget Settings. See Error Logs for More Details.");

            log.info("Some Error Occurred while Saving Queue Gadget Settings. See Error Logs for More Details.");
            log.error("Exception Occurred while Saving Queue Gadget Settings: ", ex);
        }

        render resultSet as JSON
        return;
    }
}