package chat_queue_backend

import grails.converters.JSON
import com.ef.app.sharedresources.Queue;
import static org.springframework.http.HttpStatus.*;

class GadgetQueueAttributeController {

    def index() {

        def resultSet = [:];

        log.info "Going to Fetch List of Queue Attributes with Params: ${params}"

        try {

            if (params.gadgetId) {

                String gadgetIdStr = params.gadgetId;
                int gadgetId = Integer.parseInt(gadgetIdStr);

                def queueAttributes = QueueAttribute.all;

                for (int i = 0; i < queueAttributes.size(); i++) {
                    queueAttributes[i].selected = GadgetQueueAttribute.findByGadgetIdAndQueueAttribute(gadgetId, queueAttributes[i]) ? true : false;;
                }

                log.info("List of Queue Attributes Successfully.");

                resultSet.put("status", OK.name());
                resultSet.put("queueAttributes", queueAttributes);

            } else {

                log.info("Gadget Id Not Found in the Request while Fetching List of Queue Attributes.");

                resultSet.put("message", "Gadget Id Not Found in the Request while Fetching List of Queue Attributes.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Fetching List of Queue Attributes. See Error Logs for More Details.");
            log.error(" Exception Occurred while Fetching List of Queue Attributes: ", ex);
        }

        render resultSet as JSON
        return;
    }

    def add() {

        def resultSet = [:];

        log.info "Going to Add Queue Attribute in the Gadget with Params: ${params}"

        try {

            if (params.queueAttributeId && params.gadgetId) {

                String gadgetIdStr = params.gadgetId;
                int gadgetId = Integer.parseInt(gadgetIdStr);

                String attributeIdStr = params.queueAttributeId;
                Long attributeId = Long.parseLong(attributeIdStr);

                def queueAttribute = QueueAttribute.findById(attributeId);

                if (queueAttribute) {

                    GadgetQueueAttribute gadgetQueueAttribute = GadgetQueueAttribute.findByGadgetIdAndQueueAttribute(gadgetId, queueAttribute);

                    if (gadgetQueueAttribute) {

                        resultSet.put("status", CONFLICT.name());
                        resultSet.put("gadgetQueue", "Queue Attribute: ${queueAttribute.name} Already Added in the Gadget: ${gadgetId}");

                        log.info("Queue Attribute: ${queueAttribute.name} Already Added in the Gadget: ${gadgetId}");

                    } else {

                        gadgetQueueAttribute = new GadgetQueueAttribute();

                        gadgetQueueAttribute.gadgetId = gadgetId;
                        gadgetQueueAttribute.queueAttribute = queueAttribute;

                        gadgetQueueAttribute.save(flush: true, failOnError: true);

                        resultSet.put("status", OK.name());
                        resultSet.put("gadgetQueueAttribute", gadgetQueueAttribute);

                        log.info("Queue Attribute Added in the Gadget Successfully.");
                    }

                } else {

                    resultSet.put("status", NOT_FOUND.name());
                    resultSet.put("message", "Queue Attribute Not Found with Id: ${attributeId} to Add in the Gadget.");

                    log.info("Queue Attribute Not Found with Id: ${attributeId} to Add in the Gadget.");
                }
            } else {

                log.info("All the Required Parameters Not Found in the Request to Add Queue Attribute in the Gadget.");

                resultSet.put("message", "All the Required Parameters Not Found in the Request to Add Queue Attribute in the Gadget.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Adding Queue Attribute in the Gadget. See Error Logs for More Details.");
            log.error("Exception Occurred while Adding Queue Attribute in the Gadget: ", ex);
        }

        render resultSet as JSON
        return;
    }

    def remove() {

        def resultSet = [:];

        log.info "Going to Remove Queue Attribute from the Gadget with Params: ${params}"

        try {

            if (params.queueAttributeId && params.gadgetId) {

                String gadgetIdStr = params.gadgetId;
                int gadgetId = Integer.parseInt(gadgetIdStr);

                String attributeIdStr = params.queueAttributeId;
                Long attributeId = Long.parseLong(attributeIdStr);

                def queueAttribute = QueueAttribute.findById(attributeId);

                if (queueAttribute) {

                    def gadgetQueueAttribute = GadgetQueueAttribute.findByGadgetIdAndQueueAttribute(gadgetId, queueAttribute);

                    if (gadgetQueueAttribute) {

                        gadgetQueueAttribute.queueAttribute = null;

                        gadgetQueueAttribute.delete(flush: true);

                        resultSet.put("status", OK.name());
                        resultSet.put("gadgetQueueAttribute", gadgetQueueAttribute);

                        log.info("Queue Attribute Removed from the Gadget Successfully.");

                    } else {

                        resultSet.put("status", NOT_FOUND.name());
                        resultSet.put("message", "Gadget Queue Not Found with Id: ${gadgetId} to Remove from Gadget.");

                        log.info("Gadget Queue Attribute Not Found with Id: ${gadgetId} to Remove from Gadget.");
                    }

                } else {

                    resultSet.put("status", NOT_FOUND.name());
                    resultSet.put("message", "Queue Attribute Not Found with Id: ${attributeId} to Remove from Gadget.");

                    log.info("Queue Attribute Not Found with Id: ${attributeId} to Remove from Gadget.");
                }
            } else {

                log.info("All the Required Parameters Not Found in the Request to Remove Queue Attribute from the Gadget.");

                resultSet.put("message", "All the Required Parameters Not Found in the Request to Remove Queue Attribute from the Gadget.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Removing Queue Attribute from the Gadget. See Error Logs for More Details.");
            log.error("Exception Occurred while Removing Queue Attribute from the Gadget: ", ex);
        }

        render resultSet as JSON
        return;
    }
}