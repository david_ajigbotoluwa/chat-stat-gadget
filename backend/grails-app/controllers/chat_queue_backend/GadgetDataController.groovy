package chat_queue_backend

import grails.converters.JSON
import static org.springframework.http.HttpStatus.*;

//todo: not being used for now
class GadgetDataController {

    def index() {

        def resultSet = [:];

        log.info "Going to Fetch Gadget Data with Params: ${params}"

        try {

            if (params.gadgetId) {

                String gadgetIdStr = params.gadgetId;
                int gadgetId = Integer.parseInt(gadgetIdStr);

                def gadgetQueuesList = GadgetQueue.findAllByGadgetId(gadgetId);
                def queueAttributes = GadgetQueueAttribute.findAllByGadgetId(gadgetId);
                def gadgetSetting = GadgetQueueThresholdSetting.findByGadgetId(gadgetId);

                if (gadgetSetting) {
                    resultSet.put("gadgetSetting", gadgetSetting);
                    log.info("Gadget Settings Found.");
                }

                if (gadgetQueuesList.size() > 0) {
                    resultSet.put("gadgetQueuesList", gadgetQueuesList);
                    log.info("Gadget Queues List Found: ${gadgetQueuesList.size()}");
                }

                if (queueAttributes.size() > 0) {
                    resultSet.put("gadgetQueueAttributes", queueAttributes);
                    log.info("Gadget Queues Attributes Found: ${queueAttributes.size()}");
                }

                resultSet.put("status", OK.name());

                log.info("Gadget Data Fetched Successfully.");

            } else {

                log.info("Gadget Id Not Found in the Request while Fetching Gadget Data.");

                resultSet.put("message", "Gadget Id Not Found in the Request while Fetching Gadget Data.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Fetching Gadget Data. See Error Logs for More Details.");
            log.error("Exception Occurred while Fetching Gadget Data: ", ex);
        }

        render resultSet as JSON
        return;
    }
}