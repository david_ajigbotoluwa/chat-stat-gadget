package chat_queue_backend

import grails.converters.JSON;
import com.ef.app.sharedresources.Queue;
import com.ef.app.sharedresources.QueueStats;
import static org.springframework.http.HttpStatus.*;

class GadgetQueueController {

    def index() {

        def resultSet = [:];

        log.info "Going to Fetch List of Selected Queues with Params: ${params}"

        try {

            if (params.gadgetId) {

                String gadgetIdStr = params.gadgetId;
                int gadgetId = Integer.parseInt(gadgetIdStr);

                def gadgetQueuesList = GadgetQueue.findAllByGadgetId(gadgetId);

                def queues = Queue.all;
                def queuesStats = QueueStats.all;

                for (int i = 0; i < queues.size(); i++) {
                    queues[i].selected = GadgetQueue.findByGadgetIdAndQueue(gadgetId, queues[i]) ? true : false;;
                }

                log.info("Selected Queues List Fetched Successfully.");

                resultSet.put("status", OK.name());
                resultSet.put("queuesList", queues);
                resultSet.put("queueStatsList", queuesStats);

            } else {

                log.info("Gadget Id Not Found in the Request while Fetching List of Selected Queues in the Gadget.");

                resultSet.put("message", "Gadget Id Not Found in the Request while Fetching List of Selected Queues in the Gadget.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Fetching List of Selected Queues in the Gadget. See Error Logs for More Details.");
            log.error("Exception Occurred while Fetching List of Selected Queues in the Gadget: ", ex);
        }

        render resultSet as JSON
        return;
    }

    def add() {

        def resultSet = [:];

        log.info "Going to Add Queue in the Gadget with Params: ${params}"

        try {

            if (params.queueId && params.gadgetId) {

                String gadgetIdStr = params.gadgetId;
                int gadgetId = Integer.parseInt(gadgetIdStr);

                String queueIdStr = params.queueId;
                Long queueId = Long.parseLong(queueIdStr);

                def queue = Queue.findById(queueId);

                if (queue) {

                    GadgetQueue gadgetQueue = GadgetQueue.findByGadgetIdAndQueue(gadgetId, queue);

                    if (gadgetQueue) {

                        resultSet.put("status", CONFLICT.name());
                        resultSet.put("gadgetQueue", "Queue: ${queue.name} Already Added in the Gadget: ${gadgetId}");

                        log.info("Queue: ${queue.name} Already Added in the Gadget: ${gadgetId}");

                    } else {

                        gadgetQueue = new GadgetQueue();

                        gadgetQueue.gadgetId = gadgetId;
                        gadgetQueue.queue = queue;

                        gadgetQueue.save(flush: true, failOnError: true);

                        resultSet.put("status", OK.name());
                        resultSet.put("gadgetQueue", gadgetQueue);

                        log.info("Queue Added in the Gadget Successfully.");
                    }

                } else {

                    resultSet.put("status", NOT_FOUND.name());
                    resultSet.put("message", "Queue Not Found with Id: ${queueId} to Add in the Gadget.");

                    log.info("Queue Not Found with Id: ${queueId} to Add in the Gadget.");
                }

            } else {

                log.info("All the Required Parameters Not Found in the Request to Add Queue in the Gadget.");

                resultSet.put("message", "All the Required Parameters Not Found in the Request to Add Queue in the Gadget.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Adding Queue in the Gadget. See Error Logs for More Details.");
            log.error("Exception Occurred while Adding Queue in the Gadget: ", ex);
        }

        render resultSet as JSON
        return;
    }

    def remove() {

        def resultSet = [:];

        log.info "Going to Remove Queue from the Gadget with Params: ${params}"

        try {

            if (params.queueId && params.gadgetId) {

                String gadgetIdStr = params.gadgetId;
                int gadgetId = Integer.parseInt(gadgetIdStr);

                String queueIdStr = params.queueId;
                Long queueId = Long.parseLong(queueIdStr);

                def queue = Queue.findById(queueId);

                if (queue) {

                    def gadgetQueue = GadgetQueue.findByGadgetIdAndQueue(gadgetId, queue);

                    if (gadgetQueue) {

                        gadgetQueue.queue = null;

                        gadgetQueue.delete(flush: true);

                        resultSet.put("status", OK.name());

                        log.info("Queue Removed from the Gadget Successfully.");

                    } else {

                        resultSet.put("status", NOT_FOUND.name());
                        resultSet.put("message", "Gadget Queue Not Found with Id: ${gadgetId} to Remove from Gadget.");

                        log.info("Gadget Queue Not Found with Id: ${gadgetId} to Remove from Gadget.");
                    }

                } else {

                    resultSet.put("status", NOT_FOUND.name());
                    resultSet.put("message", "Queue Not Found with Id: ${queueId} to Remove from the Gadget.");

                    log.info("Queue Not Found with Id: ${queueId} to Remove from the Gadget.");
                }

            } else {

                log.info("All the Required Parameters Not Found in the Request to Remove Queue from the Gadget.");

                resultSet.put("message", "All the Required Parameters Not Found in the Request to Remove Queue from the Gadget.");
                resultSet.put("status", BAD_REQUEST.name());
            }

        } catch (Exception ex) {

            resultSet.put("status", INTERNAL_SERVER_ERROR.name());

            log.info("Some Error Occurred while Removing Queue from the Gadget. See Error Logs for More Details.");
            log.error("Exception Occurred while Removing Queue from the Gadget: ", ex);
        }

        render resultSet as JSON
        return;
    }
}