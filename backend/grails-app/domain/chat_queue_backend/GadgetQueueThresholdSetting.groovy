package chat_queue_backend

class GadgetQueueThresholdSetting {

    //To which gadget these stats belong to
    int gadgetId;

    //Queue statistics gadget application threshold values
    int activecontact;
    int queuedcontact;
    int notReadyAgents;
    int agentsReady;
    int agentsLoggedIn;

    String thresholdColor;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        thresholdColor(nullable: true);
    }
}