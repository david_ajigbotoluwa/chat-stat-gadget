package chat_queue_backend

class GadgetQueueAttribute {

    int gadgetId;
    QueueAttribute queueAttribute;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
    }
}