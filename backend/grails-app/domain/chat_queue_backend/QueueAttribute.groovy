package chat_queue_backend

class QueueAttribute {

    String name;
    boolean selected;
    String attributeKey;
    boolean isThreshold;
    //int fieldThresholdData;

    Date dateCreated;
    Date lastUpdated;

    static transients = ['selected']

    static constraints = {
    }
}