package chat_queue_backend

import grails.converters.JSON
import com.ef.app.sharedresources.Queue;
import com.ef.app.sharedresources.QueueStats;

class BootStrap {

    def init = { servletContext ->

        //Adding Queue Attributes at the time of the initialization
        try {

            log.info("Going to Load Queue Attributes while Initializing the Application.");

            int queueAttributesAdded = QueueAttribute.count();

            log.info("Total Queue Attributes Found: ${queueAttributesAdded}")

            if (queueAttributesAdded == 0) {

                new QueueAttribute(attributeKey: "activecontact", name: "Active Contacts", isThreshold: true).save(flush: true, failOnError: true);
                new QueueAttribute(attributeKey: "agentsReady", name: "Agents: Ready", isThreshold: true).save(flush: true, failOnError: true);
                new QueueAttribute(attributeKey: "agentsLoggedIn", name: "Agents: LoggedIn", isThreshold: true).save(flush: true, failOnError: true);
                new QueueAttribute(attributeKey: "notReadyAgents", name: "Agents: Not Ready", isThreshold: true).save(flush: true, failOnError: true);
                new QueueAttribute(attributeKey: "queuedcontact", name: "Queued Contacts", isThreshold: true).save(flush: true, failOnError: true);

                log.info("Queue Attributes Loaded Successfully.");

            } else {

                log.info("Queue Attributes Already Loaded.");
            }

        } catch (Exception ex) {

            log.info("Some Error Occurred while Loading Queue Attributes. See Error Logs for More Details.");
            log.error("Exception Occurred while Loading Queue Attributes: ", ex);
        }

        //Domain Classes Marshaller

        //Queue Gadget Threshold Domain
        JSON.registerObjectMarshaller(GadgetQueueThresholdSetting) {

            def output = [:]

            output['id'] = it.id;
            output['gadgetId'] = it.gadgetId;
            output['thresholdColor'] = it.thresholdColor;

            if (it.activeemails > 0) output['activeemails'] = it.activeemails;
            if (it.agentsReady > 0) output['agentsReady'] = it.agentsReady;
            if (it.notReadyAgents > 0) output['notReadyAgents'] = it.notReadyAgents;
            if (it.offeredemails > 0) output['offeredemails'] = it.offeredemails;
            if (it.agentsLoggedIn > 0) output['agentsLoggedIn'] = it.agentsLoggedIn;
            if (it.handledemails > 0) output['handledemails'] = it.handledemails;

            return output;
        }

        //Queue Attribute Domain
        JSON.registerObjectMarshaller(QueueAttribute) {

            def output = [:];

            output['id'] = it.id;
            output['name'] = it.name;
            output['key'] = it.attributeKey;
            output['selected'] = it.selected;

            return output;
        }

        //Queue
        JSON.registerObjectMarshaller(Queue) {

            def output = [:];

            output['id'] = it.id;
            output['name'] = it.name;
            output['selected'] = it.selected;

            return output;
        }

        //Queue Stats
        JSON.registerObjectMarshaller(QueueStats) {

            def output = [:];

            output['id'] = it.id;
            output['queueId'] = it.queue.id;
            output['queueName'] = it.queue.name;
            output['agentsReady'] = it.agentsReady;
            output['notReadyAgents'] = it.agentsNotReady;
            output['agentsLoggedIn'] = it.agentsLoggedIn;
            output['activeemails'] = it.activeemails;
            output['handledemails'] = it.handledemails;
            output['offeredemails'] = it.offeredemails;


            return output;
        }
    }
    def destroy = {
    }
}