(function () {
    'use strict';

    angular
        .module('app.queueStats')
        .directive('validateThreshold', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {

                    console.log("Threshold Value Attributes: " + JSON.stringify(attrs));

                    //console.log("Flex Value Attributes: " + document.getElementById("dashboard-project").getAttribute('flex'));
                }
            };
        })
        .filter('secondsToDateTime', function () {
            return function (seconds) {
                var d = new Date(0, 0, 0, 0, 0, 0, 0);
                d.setSeconds(seconds);
                return d;
            };
        });
})();