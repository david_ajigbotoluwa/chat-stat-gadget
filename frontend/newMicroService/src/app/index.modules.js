angular.module('queue.stats',[
        'app.queueStats'
    ]
);
angular.module("fuse").requires.push("queue.stats");

window.wallboardAppBaseUrl = 'http://localhost:8080';
window.queueGadgetAppBaseUrl = 'http://localhost:9090/chatqueuegadget';