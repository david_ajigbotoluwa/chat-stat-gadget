(function () {

    'use strict';

    angular
        .module('app.queueStats', [
            // 3rd Party Dependencies
            'ngMaterial'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {

        $stateProvider

        //Main State
            .state('app.queueStats', {
                abstract: true,
                url: '/queueGadget'
            })

            //Home State to show dashboards
            .state('app.queueStats.home', {
                url: '/stats',
                views: {
                    'content@app': {
                        templateUrl: 'app/views/queueStats/chatqueue-stats.html',
                        controller: 'QueueStatsController as vm'
                    }
                }
            });

        $translatePartialLoaderProvider.addPart('/translations/queueStatsGadgets/i18n');

        // Navigation
        msNavigationServiceProvider.saveItem('queueStats', {
            title: 'queue.queueStatsGadget',
            icon: 'icon-cog',
            state: 'app.queueStats.home',
            weight: 9
        });
    }
})();