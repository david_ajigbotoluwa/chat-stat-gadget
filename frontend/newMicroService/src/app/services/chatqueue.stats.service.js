(function () {
    'use strict';
    angular
        .module('app.queueStats')
        .factory('queueStatsService', ['$resource', '$q', queueStatsService]);

    function queueStatsService($resource, $q) {

        var chatqueueGadgetResource = $resource('../chatqueuegadget/', {id: '@id'}, {

            settings: {
                method: 'GET',
                url: window.queueGadgetAppBaseUrl + '/gadgetQueueThresholdSetting/',
                params: {
                    gadgetId: '@gadgetId'
                }
            },
            saveSettings: {
                method: 'POST',
                url: window.queueGadgetAppBaseUrl + '/gadgetQueueThresholdSetting/saveSetting/',
                params: {
                    activeemails: '@activeemails',
                    gadgetId: '@gadgetId',
                    handledemails: '@handledemails',
                    notReadyAgents: '@notReadyAgents',
                    offeredemails: '@offeredemails',
                    thresholdColor: '@thresholdColor',
                    agentsLoggedIn: '@agentsLoggedIn',
                    agentsReady:'@agentsReady'
                }
            },
            queues: {
                method: 'GET',
                url: window.queueGadgetAppBaseUrl + '/gadgetQueue/',
                params: {
                    gadgetId: '@gadgetId'
                }
            },
            queueAttributes: {
                method: 'GET',
                url: window.queueGadgetAppBaseUrl + '/gadgetQueueAttribute/',
                params: {
                    gadgetId: '@gadgetId'
                }
            },
            addQueue: {
                method: 'POST',
                url: window.queueGadgetAppBaseUrl + '/gadgetQueue/add/',
                params: {
                    queueId: '@queueId',
                    gadgetId: '@gadgetId'
                }
            },
            removeQueue: {
                method: 'POST',
                url: window.queueGadgetAppBaseUrl + '/gadgetQueue/remove/',
                params: {
                    queueId: '@queueId',
                    gadgetId: '@gadgetId'
                }
            },
            addQueueAttribute: {
                method: 'POST',
                url: window.queueGadgetAppBaseUrl + '/gadgetQueueAttribute/add/',
                params: {
                    gadgetId: '@gadgetId',
                    queueAttributeId: '@queueAttributeId'
                }
            },
            removeQueueAttribute: {
                method: 'POST',
                url: window.queueGadgetAppBaseUrl + '/gadgetQueueAttribute/remove/',
                params: {
                    gadgetId: '@gadgetId',
                    queueAttributeId: '@queueAttributeId'
                }
            }
        });

        var queueStatService = {

            //Queues Manipulation
            addQueue: addQueue,
            getQueues: getQueues,
            removeQueue: removeQueue,

            //Queues Attributes Manipulation
            getQueueAttributes: getQueueAttributes,
            addQueueAttribute: addQueueAttribute,
            removeQueueAttribute: removeQueueAttribute,

            //Gadget Threshold Settings Manipulation
            getQueueGadgetSettings: getQueueGadgetSettings,
            saveQueueGadgetSettings: saveQueueGadgetSettings
        };

        function getQueueGadgetSettings(params) {

            //Create a new deferred object
            var deferred = $q.defer();

            chatqueueGadgetResource.settings(params, function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function saveQueueGadgetSettings(params) {

            //Create a new deferred object
            var deferred = $q.defer();

            chatqueueGadgetResource.saveSettings(params, function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function getQueues(params) {

            //Create a new deferred object
            var deferred = $q.defer();

            chatqueueGadgetResource.queues(params, function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function addQueue(params) {

            //Create a new deferred object
            var deferred = $q.defer();

            chatqueueGadgetResource.addQueue(params, function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function removeQueue(params) {

            //Create a new deferred object
            var deferred = $q.defer();

            chatqueueGadgetResource.removeQueue(params, function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function getQueueAttributes(params) {

            //Create a new deferred object
            var deferred = $q.defer();

            chatqueueGadgetResource.queueAttributes(params, function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function addQueueAttribute(params) {

            //Create a new deferred object
            var deferred = $q.defer();

            chatqueueGadgetResource.addQueueAttribute(params, function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function removeQueueAttribute(params) {

            //Create a new deferred object
            var deferred = $q.defer();

            chatqueueGadgetResource.removeQueueAttribute(params, function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        return queueStatService;
    }

})();