(function () {
    'use strict';
    angular
        .module('app.queueStats')
        .factory('defaultQueueApplicationSettingService', ['$resource', '$q', defaultQueueApplicationSettingService]);

    function defaultQueueApplicationSettingService($resource, $q) {

        var defaultQueueApplicationSettingService = $resource('../applicationSetting/', {id: '@id'}, {

            getDefaultSettings: {
                method: 'GET',
                url: window.wallboardAppBaseUrl + '/applicationSetting/'
            }
        });

        return {

            'getDefaultSettings': function () {

                var defered = $q.defer();

                defaultQueueApplicationSettingService.getDefaultSettings(function (response) {
                        defered.resolve(response);
                    },
                    function (error) {
                        defered.reject(error);
                    });

                return defered.promise;
            }
        }
    }
})();