(function () {
    'use strict';

    angular
        .module('app.queueStats')
        .controller('QueueSettingsController', QueueSettingsController);

    /** @ngInject */
    function QueueSettingsController($mdDialog, gadgetId, queuesList, queueAttributesList, queueStats, selectedQueuesList, gadgetSetting,
                                     finalQueueGadgetSetting, selectedQueueAttributesList, utilCustom,
                                     queueStatsService, defaultQueueApplicationSettingService) {

        var vm = this;

        vm.searchQueueText = '';
        vm.queueStats = queueStats;
        vm.queuesList = queuesList;
        vm.gadgetSetting = gadgetSetting;
        vm.selectedQueuesList = selectedQueuesList;
        vm.queueAttributesList = queueAttributesList;
        vm.finalQueueGadgetSetting = finalQueueGadgetSetting;
        vm.selectedQueueAttributesList = selectedQueueAttributesList;

        vm.init = init;
        vm.closeDialog = closeDialog;
        vm.updateQueues = updateQueues;
        vm.queueQuerySearch = queueQuerySearch;
        vm.saveGadgetSetting = saveGadgetSetting;
        vm.updateQueueAttributes = updateQueueAttributes;
        vm.queueAttributeQuerySearch = queueAttributeQuerySearch;
        vm.loadDefaultGadgetSettings = loadDefaultGadgetSettings;
        vm.syncGadgetThresholdSettings = syncGadgetThresholdSettings;

        const gadgetIdPassed = 1;//gadgetId;
        function init() {
            vm.loadDefaultGadgetSettings();
        }

        init();

        function loadDefaultGadgetSettings() {

            console.log("Going to Fetch Default Application Settings.");

            defaultQueueApplicationSettingService.getDefaultSettings().then(function (response) {

                if (response.status === 'OK') {
                    vm.defaultGadgetSetting = angular.copy(response.applicationSetting);
                    console.log("Default Application Settings Fetched Successfully!");
                } else {
                    vm.defaultGadgetSetting = {};
                    utilCustom.toaster("Some Error Occurred while Fetching Default Application Settings.");
                    console.log("Some Error Occurred while Fetching Default Application Settings with Status: " + response.status);
                }
            }, function (error) {
                vm.defaultGadgetSetting = {};
                utilCustom.toaster("Some Error Occurred while Fetching Default Application Settings.");
                console.log("Some Error Occurred while Fetching Default Application Settings: " + JSON.stringify(error));
            });
        }

        //Syncing Gadget Threshold Settings with Default Gadget Settings
        function syncGadgetThresholdSettings() {

            vm.finalQueueGadgetSetting = angular.copy(vm.gadgetSetting);

            if ((!vm.gadgetSetting.activeemails || vm.gadgetSetting.activeemails === "") && vm.defaultGadgetSetting.activeemails) {
                vm.finalQueueGadgetSetting.activeemails = vm.defaultGadgetSetting.activeemails
            }

            if ((!vm.gadgetSetting.handledemails || vm.gadgetSetting.handledemails === "") && vm.defaultGadgetSetting.handledemails) {
                vm.finalQueueGadgetSetting.handledemails = vm.defaultGadgetSetting.handledemails
            }

            if ((!vm.gadgetSetting.offeredemails || vm.gadgetSetting.offeredemails === "") && vm.defaultGadgetSetting.offeredemails) {
                vm.finalQueueGadgetSetting.offeredemails = vm.defaultGadgetSetting.offeredemails
            }

            if ((!vm.gadgetSetting.notReadyAgents || vm.gadgetSetting.notReadyAgents === "") && vm.defaultGadgetSetting.notReadyAgents) {
                vm.finalQueueGadgetSetting.notReadyAgents = vm.defaultGadgetSetting.notReadyAgents
            }

            if ((!vm.gadgetSetting.agentsLoggedIn || vm.gadgetSetting.agentsLoggedIn === "") && vm.defaultGadgetSetting.agentsLoggedIn  ) {
                vm.finalQueueGadgetSetting.agentsLoggedIn = vm.defaultGadgetSetting.agentsLoggedIn
            }

            if ((!vm.gadgetSetting.agentsReady || vm.gadgetSetting.agentsReady === "") && vm.defaultGadgetSetting.agentsReady  ) {
                vm.finalQueueGadgetSetting.agentsReady = vm.defaultGadgetSetting.agentsReady
            }

            if ((!vm.gadgetSetting.thresholdColor || vm.gadgetSetting.thresholdColor === "") && vm.defaultGadgetSetting.thresholdColor) {
                vm.finalQueueGadgetSetting.thresholdColor = vm.defaultGadgetSetting.thresholdColor
            }

            console.log("Final Gadget Settings: " + JSON.stringify(vm.finalQueueGadgetSetting));
        }

        //Gadget Settings
        function saveGadgetSetting(gadgetSetting) {

            console.log("Gadget Id is: " + gadgetIdPassed);
            console.log("Going to Save Gadget Settings: " + JSON.stringify(gadgetSetting));

            var params = {
                gadgetId: gadgetIdPassed,
                activeemails: gadgetSetting.activeemails,
                handledemails: gadgetSetting.handledemails,
                thresholdColor: gadgetSetting.thresholdColor,
                offeredemails: gadgetSetting.offeredemails,
                notReadyAgents: gadgetSetting.notReadyAgents,
                agentsReady: gadgetSetting.agentsReady,
                agentsLoggedIn: gadgetSetting.agentsLoggedIn
            };

            queueStatsService.saveQueueGadgetSettings(params).then(function (response) {

                if (response.status === 'OK') {

                    vm.gadgetSetting = response.applicationSetting;
                    console.log("Gadget Settings Saved Successfully.");

                    utilCustom.toaster("Queue Gadget Settings Saved Successfully.");

                    //Syncing default and gadget settings
                    vm.syncGadgetThresholdSettings();
                } else {
                    vm.gadgetSetting = {};
                    utilCustom.toaster("Some Error Occurred while Saving Queue Gadget Settings.");
                    console.log("Gadget Settings NOT Saved Successfully with status: " + response.status);
                }
            }, function (error) {
                vm.gadgetSetting = {};
                utilCustom.toaster("Some Error Occurred while Saving Queue Gadget Settings.");
                console.log("Some Error Occurred while Saving Gadget Settings: " + JSON.stringify(error));
            });
        }

        /////////////////////////*******Queue Attributes Manipulation***********////////////////////

        function updateQueueAttributes(queueAttributeObject, updateFromMenue, addQueueAttribute) {

            if (queueAttributeObject != undefined) {

                var params = {
                    gadgetId: gadgetIdPassed,
                    queueAttributeId: queueAttributeObject.id
                };

                var operationPerformed = false;

                if (addQueueAttribute) {

                    queueStatsService.addQueueAttribute(params).then(function (response) {

                        if (response.status === "OK") {

                            operationPerformed = true;

                            if (updateFromMenue) {
                                vm.selectedQueueAttributesList.push(queueAttributeObject);
                            }

                            //Update Queue stats
                            for (var i = 0; i < vm.queueAttributesList.length; i++) {
                                if (vm.queueAttributesList[i].name === queueAttributeObject.name) {
                                    vm.queueAttributesList[i].selected = true;
                                }
                            }

                            utilCustom.toaster("Queue Attribute Added Successfully in the Gadget.");
                            console.log("Queue Attribute Added Successfully in the Gadget.");

                        } else {
                            utilCustom.toaster("Some Error Occurred while Adding Queue Attribute in the Gadget.");
                            console.log("Some Error Occurred while Adding Queue Attribute in the Gadget: " + JSON.stringify(response));
                        }

                    }, function (error) {
                        utilCustom.toaster("Some Error Occurred while Adding Queue Attribute in the Gadget.");
                        console.log("Some Error Occurred while Adding Queue Attribute in the Gadget: " + JSON.stringify(error));
                    });

                } else {

                    queueStatsService.removeQueueAttribute(params).then(function (response) {

                        if (response.status === "OK") {

                            operationPerformed = true;

                            if (updateFromMenue) {

                                var index = -1;
                                for (var i = 0; i < vm.selectedQueueAttributesList.length; i++) {
                                    if (vm.selectedQueueAttributesList[i].name === queueAttributeObject.name) {
                                        index = i;
                                        break;
                                    }
                                }

                                if (index != -1) {
                                    vm.selectedQueueAttributesList.splice(index, 1);
                                }
                            }

                            //Update Queue stats
                            for (var i = 0; i < vm.queueAttributesList.length; i++) {
                                if (vm.queueAttributesList[i].name === queueAttributeObject.name) {
                                    vm.queueAttributesList[i].selected = false;
                                }
                            }

                            utilCustom.toaster("Queue Attribute Remove Successfully from the Gadget.");
                            console.log("Queue Attribute Remove Successfully from the Gadget.");

                        } else {
                            utilCustom.toaster("Some Error Occurred while Removing Queue Attribute from the Gadget.");
                            console.log("Some Error Occurred while Removing Queue Attribute from the Gadget: " + JSON.stringify(response));
                        }

                    }, function (error) {
                        utilCustom.toaster("Some Error Occurred while Removing Queue Attribute from the Gadget.");
                        console.log("Some Error Occurred while Removing Queue Attribute from the Gadget: " + JSON.stringify(error));
                    });
                }

                return operationPerformed;

            } else {
                return false;
            }
        }

        function queueAttributeQuerySearch(query) {
            return query ? vm.queueAttributesList.filter(createFilterForQueueAttribute(query)) : [];
        }

        function createFilterForQueueAttribute(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(item) {
                var result = angular.lowercase(item.name).indexOf(lowercaseQuery) >= 0 && item.selected === false;
                return result;
            };
        }

        /////////////////////////*******Queue Lists Manipulation***********////////////////////

        //When queue is selected / unselected either from text search drop down or from menu bar
        function updateQueues(queueObject, updateFromMenue, addQueue) {

            if (queueObject != undefined) {

                var params = {
                    gadgetId: gadgetIdPassed,
                    queueId: queueObject.id
                };

                var operationPerformed = false;

                if (addQueue) {

                    queueStatsService.addQueue(params).then(function (response) {

                        if (response.status === "OK") {

                            operationPerformed = true;

                            if (updateFromMenue) {
                                vm.selectedQueuesList.push(queueObject);
                            }

                            //Update Queue stats
                            for (var i = 0; i < vm.queueStats.length; i++) {

                                if (vm.queueStats[i].queueName === queueObject.name) {
                                    vm.queueStats[i].selected = true;
                                }

                                if (vm.queuesList[i].name === queueObject.name) {
                                    vm.queuesList[i].selected = true;
                                }
                            }

                            utilCustom.toaster("Queue Added Successfully in the Gadget.");
                            console.log("Queue Added Successfully in the Gadget.");

                        } else {
                            utilCustom.toaster("Some Error Occurred while Adding Queue in the Gadget.");
                            console.log("Some Error Occurred while Adding Queue in the Gadget: " + JSON.stringify(response));
                        }

                    }, function (error) {

                        utilCustom.toaster("Some Error Occurred while Adding Queue in the Gadget.");
                        console.log("Some Error Occurred while Adding Queue in the Gadget: " + JSON.stringify(error));
                    });

                } else {

                    queueStatsService.removeQueue(params).then(function (response) {

                        if (response.status === "OK") {

                            operationPerformed = true;

                            if (updateFromMenue) {

                                var index = -1;
                                for (var i = 0; i < vm.selectedQueuesList.length; i++) {
                                    if (vm.selectedQueuesList[i].name === queueObject.name) {
                                        index = i;
                                        break;
                                    }
                                }

                                if (index != -1) {
                                    vm.selectedQueuesList.splice(index, 1);
                                }
                            }

                            //Update Queue stats
                            for (var i = 0; i < vm.queueStats.length; i++) {

                                if (vm.queueStats[i].queueName === queueObject.name) {
                                    vm.queueStats[i].selected = false;
                                }

                                if (vm.queuesList[i].name === queueObject.name) {
                                    vm.queuesList[i].selected = false;
                                }
                            }

                            utilCustom.toaster("Queue Remove Successfully from the Gadget.");
                            console.log("Queue Remove Successfully from the Gadget.");

                        } else {
                            utilCustom.toaster("Some Error Occurred while Removing Queue from the Gadget.");
                            console.log("Some Error Occurred while Removing Queue from the Gadget: " + JSON.stringify(response));
                        }

                    }, function (error) {
                        utilCustom.toaster("Some Error Occurred while Removing Queue from the Gadget.");
                        console.log("Some Error Occurred while Removing Queue from the Gadget: " + JSON.stringify(error));
                    });
                }

                return operationPerformed;

            } else {
                return false;
            }
        }

        function queueQuerySearch(query) {
            return query ? vm.queuesList.filter(createFilterForQueue(query)) : [];
        }

        function createFilterForQueue(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(item) {
                var result = angular.lowercase(item.name).indexOf(lowercaseQuery) >= 0 && item.selected === false;
                return result;
            };
        }

        /////////////////////////*******Close Dialog***********////////////////////

        function closeDialog() {
            //todo: There is an ISSUE. If user change threshold settings and close the dialogue box immediately then settings
            //aren't applied because before it update the model(finalQueueGadgetSetting) it sends back it to the controller
            //This scenario specially occurs when backend application is down
            $mdDialog.hide({
                "selectedQueueAttributesList": selectedQueueAttributesList,
                "finalQueueGadgetSetting": vm.finalQueueGadgetSetting
            });
        }
    }
})();