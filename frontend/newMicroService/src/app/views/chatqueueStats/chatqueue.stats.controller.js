(function () {
    'use strict';

    angular
        .module('app.queueStats')
        .controller('QueueStatsController', QueueStatsController);

    /** @ngInject */
    function QueueStatsController($scope, $mdDialog, $document, queueStatsService,
                                  defaultQueueApplicationSettingService, utilCustom) {

        var vm = this;
        vm.queuesList = [];
        vm.queueStats = [];
        vm.gadgetSetting = {};
        vm.selectedQueuesList = [];
        vm.queueAttributesList = [];
        vm.defaultGadgetSetting = {};
        vm.finalQueueGadgetSetting = {};
        vm.selectedQueueAttributesList = [];

        vm.init = init;
        vm.getColor = getColor;
        vm.showQueue = showQueue;
        vm.showAttribute = showAttribute;
        vm.loadQueueGadgetSettings = loadQueueGadgetSettings;
        vm.loadDefaultGadgetSettings = loadDefaultGadgetSettings;
        vm.syncGadgetThresholdSettings = syncGadgetThresholdSettings;

        const gadgetId = 1;//$scope.gadgetId;

        function init() {

            console.log("Going to Fetch Queues List with Gadget Id: " + gadgetId);

            var params = {
                gadgetId: gadgetId
            };

            queueStatsService.getQueues(params).then(function (response) {

                if (response.status === "OK") {

                    vm.queuesList = response.queuesList;
                    vm.queueStats = response.queueStatsList;

                    if (vm.selectedQueuesList.length === 0) {
                        for (var i = 0; i < vm.queuesList.length; i++) {
                            if (vm.queuesList[i].selected === true) {
                                vm.selectedQueuesList.push(angular.copy(vm.queuesList[i]));
                            }
                        }
                    }

                    console.log("Queues List Fetched Successfully.");

                } else {
                    console.log("Some Error Occurred while Fetching List of Queues: " + JSON.stringify(response));
                }

            }, function (error) {
                console.log("Some Error Occurred while Fetching List of Queues: " + JSON.stringify(error));
            });

            console.log("Going to Fetch Queue Attributes List");

            queueStatsService.getQueueAttributes(params).then(function (response) {

                if (response.status === "OK") {

                    vm.queueAttributesList = response.queueAttributes;

                    if (vm.selectedQueueAttributesList.length === 0) {
                        for (var i = 0; i < vm.queueAttributesList.length; i++) {
                            if (vm.queueAttributesList[i].selected === true) {
                                vm.selectedQueueAttributesList.push(angular.copy(vm.queueAttributesList[i]));
                            }
                        }
                    }

                    console.log("Queues Attributes List Fetched Successfully: " + JSON.stringify(vm.queueAttributesList));

                } else {
                    console.log("Some Error Occurred while Fetching List of Queues Attributes: " + response.status);
                }
            }, function (error) {
                console.log("Some Error Occurred while Fetching List of Queues Attributes: " + JSON.stringify(error));
            });

            console.log("Going to Fetch Queue Gadget Application Settings");

            queueStatsService.getQueueGadgetSettings(params).then(function (response) {

                if (response.status === 'OK') {
                    vm.gadgetSetting = response.applicationSetting;
                    console.log("Queues Gadget Settings Fetched Successfully.");
                } else {
                    vm.gadgetSetting = {};
                    console.log("Gadget Settings NOT Fetched Successfully with status: " + response.status);
                }

                vm.loadDefaultGadgetSettings();

            }, function (error) {

                vm.gadgetSetting = {};
                console.log("Some Error Occurred while Fetching Queues Gadget Settings: " + JSON.stringify(error));

                vm.loadDefaultGadgetSettings();
            });
        }

        vm.init();

        function showAttribute(attribute) {
            for (var i = 0; i < vm.selectedQueueAttributesList.length; i++) {
                if (vm.selectedQueueAttributesList[i].key === attribute) {
                    return true;
                }
            }
            return false;
        }

        function showQueue(queue) {
            for (var i = 0; i < vm.selectedQueuesList.length; i++) {
                if (vm.selectedQueuesList[i].name === queue) {
                    return true;
                }
            }
            return false;
        }

        function loadDefaultGadgetSettings() {

            console.log("Going to Fetch Default Application Settings.");

            defaultQueueApplicationSettingService.getDefaultSettings().then(function (response) {

                if (response.status === 'OK') {
                    vm.defaultGadgetSetting = angular.copy(response.applicationSetting);
                    console.log("Default Application Settings Fetched: " + JSON.stringify(response));
                } else {
                    vm.defaultGadgetSetting = {};
                    utilCustom.toaster("Some Error Occurred while Fetching Default Application Settings.");
                    console.log("Some Error Occurred while Fetching Default Application Settings with Status: " + response.status);
                }

                //Syncing application and gadget settings
                vm.syncGadgetThresholdSettings();

            }, function (error) {

                vm.defaultGadgetSetting = {};
                utilCustom.toaster("Some Error Occurred while Fetching Default Application Settings.");
                console.log("Error while Fetching Default Application Settings: " + JSON.stringify(error));

                //Syncing application and gadget settings
                vm.syncGadgetThresholdSettings();
            });
        }

        //Syncing Gadget Threshold Settings with Default Gadget Settings
        function syncGadgetThresholdSettings() {

            vm.finalQueueGadgetSetting = angular.copy(vm.gadgetSetting);

            if ((!vm.gadgetSetting.activeemails || vm.gadgetSetting.activeemails === "") && vm.defaultGadgetSetting.activeemails) {
                vm.finalQueueGadgetSetting.activeemails = vm.defaultGadgetSetting.activeemails
            }

            if ((!vm.gadgetSetting.handledemails || vm.gadgetSetting.handledemails === "") && vm.defaultGadgetSetting.handledemails) {
                vm.finalQueueGadgetSetting.handledemails = vm.defaultGadgetSetting.handledemails
            }

            if ((!vm.gadgetSetting.offeredemails || vm.gadgetSetting.offeredemails === "") && vm.defaultGadgetSetting.offeredemails) {
                vm.finalQueueGadgetSetting.offeredemails = vm.defaultGadgetSetting.offeredemails
            }

            if ((!vm.gadgetSetting.notReadyAgents || vm.gadgetSetting.notReadyAgents === "") && vm.defaultGadgetSetting.notReadyAgents) {
                vm.finalQueueGadgetSetting.notReadyAgents = vm.defaultGadgetSetting.notReadyAgents
            }

            if ((!vm.gadgetSetting.agentsLoggedIn || vm.gadgetSetting.agentsLoggedIn === "") && vm.defaultGadgetSetting.agentsLoggedIn) {
                vm.finalQueueGadgetSetting.agentsLoggedIn = vm.defaultGadgetSetting.agentsLoggedIn
            }

            if ((!vm.gadgetSetting.thresholdColor || vm.gadgetSetting.thresholdColor === "") && vm.defaultGadgetSetting.thresholdColor) {
                vm.finalQueueGadgetSetting.thresholdColor = vm.defaultGadgetSetting.thresholdColor
            }

            console.log("Final Gadget Settings: " + JSON.stringify(vm.finalQueueGadgetSetting));
        }

        function getColor(widget) {
            if (vm.finalQueueGadgetSetting[widget.attribute]) {
                return "red";
            } else {
                return "orange";
            }
        }

        function loadQueueGadgetSettings(event) {

            console.log("Going to Load Settings Dialogue.");

            try {
                $mdDialog.show({

                    controller: 'QueueSettingsController',
                    controllerAs: 'vm',
                    templateUrl: 'app/views/queueGadgetSettings/chatqueue.gadget.setting.html',
                    parent: angular.element($document.body),
                    targetEvent: event,
                    clickOutsideToClose: true,
                    locals: {
                        gadgetId: gadgetId,
                        queuesList: vm.queuesList,
                        queueStats: vm.queueStats,
                        gadgetSetting: vm.gadgetSetting,
                        selectedQueuesList: vm.selectedQueuesList,
                        queueAttributesList: vm.queueAttributesList,
                        finalQueueGadgetSetting: vm.finalQueueGadgetSetting,
                        selectedQueueAttributesList: vm.selectedQueueAttributesList
                    }

                }).then(function (res) {
                    vm.finalQueueGadgetSetting = angular.copy(res.finalQueueGadgetSetting);
                    vm.selectedQueueAttributesList = angular.copy(res.selectedQueueAttributesList);
                    console.log("Create Wallboard Settings Dialogue Closed.");
                });

            } catch (error) {
                console.log("ERROR:: " + error);
                console.log("ERROR:: " + JSON.stringify(error));
            }
        }
    }
})();